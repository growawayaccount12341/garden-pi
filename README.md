# Garden-Pi
  
### Python 3.6 Dependencies
- Adafruit-DHT
- RPi.GPIO
- Twisted

### Usage
- `python3 garden.py`
- Go to `http://{{RPI_IP_ADDRESS}}:8080`

### Tutorials Used

How to set up the DHT22 sensor on RPi
- https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/  

How to control a 5v fan on a RPi
- https://www.raspberrypi.org/forums/viewtopic.php?t=194621  

How to control power outlets from RPi
- http://www.undr.com/understatement/2018/raspi_zero_outlet_controllers/index.html  
- https://www.amazon.com/exec/obidos/ASIN/B00WV7GMA2 (Safer alternative)

    