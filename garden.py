import datetime
import json
import logging
import sys
from logging import handlers
from time import sleep

import Adafruit_DHT
import RPi.GPIO as GPIO
from twisted.internet import task, reactor, endpoints
from twisted.web.resource import Resource
from twisted.web.server import Site
from twisted.web.static import File


class Plan:

    def __init__(self, hour_on: int = 0, minute_on: int = 0, hour_off: int = 0, minute_off: int = 0,
                 always_on: bool = False, always_off: bool = False) -> None:
        self.hour_on: int = hour_on
        self.minute_on: int = minute_on
        self.hour_off: int = hour_off
        self.minute_off: int = minute_off
        self.always_on: int = always_on
        self.always_off: int = always_off

    def should_be_on(self) -> bool:
        if self.always_off:
            return False
        if self.always_on:
            return True
        now: datetime.datetime = datetime.datetime.now()
        turn_on_at: datetime.datetime = now.replace(hour=self.hour_on, minute=self.minute_on, second=0)
        turn_off_at: datetime.datetime = now.replace(hour=self.hour_off, minute=self.minute_off, second=0)
        return turn_on_at <= now < turn_off_at


class PlugToggleResource(Resource):
    isLeaf = True

    def __init__(self, plug):
        super().__init__()
        self.plug = plug

    def render_GET(self, request):
        request.setResponseCode(200)
        self.plug.switch(not self.plug.state())
        json_str = json.dumps({"state": self.plug.state_str_on_off()})
        return json_str.encode("utf-8")


class PlugResource(Resource):

    def __init__(self, plug):
        super().__init__()
        self.plug = plug
        self.putChild(b"toggle", PlugToggleResource(self.plug))

    def render_GET(self, request):
        request.setResponseCode(200)
        json_str = json.dumps({"state": self.plug.state_str_on_off()})
        return json_str.encode("utf-8")


class DHT22Resource(Resource):

    def __init__(self, name, gpio_pin, sync=True):
        super().__init__()
        self.name = name
        self.gpio_pin = gpio_pin
        self.temperature = 0.0
        self.humidity = 0.0
        self.log_cnt = 0
        if sync:
            self._sync()

    def _sync(self):
        humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, self.gpio_pin)
        if humidity is not None and temperature is not None:
            self.humidity = humidity
            self.temperature = temperature * 9 / 5.0 + 32  # convert to freedom units
        else:
            logging.error(f"{self.name} sensor failed.")

    def get_json(self):
        return json.dumps({"temp": f"{self.temperature:0.1f}",
                           "humidity": f"{self.humidity:0.1f}"}).encode("utf-8")

    def log(self):
        self._sync()
        self.log_cnt = self.log_cnt + 1
        if self.log_cnt == 7:
            logging.info(json.dumps({"name": f"{self.name}",
                                     "temp": f"{self.temperature:0.1f}",
                                     "humidity": f"{self.humidity:0.1f}"}))
            self.log_cnt = 0

    def render_GET(self, request):
        request.setResponseCode(200)
        return self.get_json()


class HumidityPlan(Plan):

    def __init__(self, dht22: DHT22Resource, target_humidity: float, max_temp: float) -> None:
        super().__init__()
        self.dht22 = dht22
        self.target_humidity = target_humidity
        self.max_temp = max_temp

    def should_be_on(self) -> bool:
        if self.dht22.humidity == 0.0:
            return True
        if self.dht22.temperature > self.max_temp:
            return True
        return True if self.dht22.humidity > self.target_humidity else False


class TemperaturePlan(Plan):

    def __init__(self, dht22: DHT22Resource, lowest_temp: float) -> None:
        super().__init__()
        self.dht22 = dht22
        self.lowest_temp = lowest_temp

    def should_be_on(self) -> bool:
        if self.dht22.temperature == 0.0:
            return False
        return True if self.dht22.temperature < self.lowest_temp else False


class Plug:

    def __init__(self, name, gpio_pin, interval, plans, reverse=False) -> None:
        self.name = name
        self.gpio_pin = gpio_pin
        self.interval = interval
        self.plans = plans
        self.reverse = reverse

    def check(self):
        logging.debug("Checking GPIO %s", self)
        try:
            switch_on = False
            if type(self.plans) == tuple:
                for p in self.plans:
                    switch_on = switch_on or p.should_be_on()
            elif isinstance(self.plans, Plan):
                switch_on = switch_on or self.plans.should_be_on()
            else:
                logging.error("Plans type unknown.")
            self.switch(switch_on)
        except:
            logging.exception("Check Error %s", self)

    def switch(self, switch_on):
        on = self.state()
        logging.debug(f"%s is {'On' if on else 'Off'}", self)
        if on and not switch_on:
            logging.info(f"Turning off %s", self)
            GPIO.output(self.gpio_pin, GPIO.LOW if self.reverse else GPIO.HIGH)  # turn off
        elif not on and switch_on:
            logging.info(f"Turning on %s", self)
            GPIO.output(self.gpio_pin, GPIO.HIGH if self.reverse else GPIO.LOW)  # turn on

    def state(self) -> bool:
        state = not GPIO.input(self.gpio_pin)
        if self.reverse:
            state = not state
        return state

    def state_str_on_off(self) -> str:
        state = self.state()
        return 'on' if state else 'off'

    def start(self):
        loop = task.LoopingCall(self.check)
        loop.start(self.interval)
        sleep(3)

    def __str__(self):
        return self.name


log = logging.getLogger()
log.setLevel(logging.INFO)
format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(format)
log.addHandler(ch)

fh = handlers.RotatingFileHandler("garden-pi.log", maxBytes=(1048576 * 2), backupCount=7)
fh.setFormatter(format)
log.addHandler(fh)

logging.info("Starting Garden-Pi")

GPIO.setmode(GPIO.BCM)

GPIO_PINS: tuple = (22, 23, 25, 5, 26, 13)

GPIO.setup(GPIO_PINS, GPIO.OUT, initial=GPIO.HIGH)

MINUTES_5: float = 300.0

try:
    inside = DHT22Resource("Inside", gpio_pin=17)
    inside_sensor_loop = task.LoopingCall(inside.log)
    inside_sensor_loop.start(11.0)

    outside = DHT22Resource("Outside", gpio_pin=4)
    outside_sensor_loop = task.LoopingCall(outside.log)
    outside_sensor_loop.start(11.0)

    plug1_plan: Plan = HumidityPlan(inside, target_humidity=60.0, max_temp=80.0)
    plug1: Plug = Plug(name="exhaust fans/air pump",
                       gpio_pin=GPIO_PINS[0],
                       interval=MINUTES_5,
                       plans=plug1_plan)

    plug2_plan: Plan = Plan(hour_on=8, hour_off=19)
    plug2: Plug = Plug(name="outside light",
                       gpio_pin=GPIO_PINS[1],
                       interval=MINUTES_5,
                       plans=plug2_plan)

    plug3_plan: Plan = TemperaturePlan(inside, lowest_temp=66.0)
    plug3: Plug = Plug(name="heating mat",
                       gpio_pin=GPIO_PINS[2],
                       interval=MINUTES_5 * 2.0,
                       plans=plug3_plan)

    plug4_plans = (Plan(hour_on=18, hour_off=23, minute_off=59), Plan(hour_on=0, minute_on=1, hour_off=6))
    plug4: Plug = Plug(name="aquarium light",
                       gpio_pin=GPIO_PINS[3],
                       interval=MINUTES_5,
                       plans=plug4_plans)

    always_on_plan = Plan(always_on=True)
    plug5: Plug = Plug(name="fan1",
                       gpio_pin=GPIO_PINS[5],
                       interval=MINUTES_5 * 2.0,
                       plans=always_on_plan,
                       reverse=True)

    plug6: Plug = Plug(name="fan2",
                       gpio_pin=GPIO_PINS[4],
                       interval=MINUTES_5 * 2.0,
                       plans=always_on_plan,
                       reverse=True)

    root = File("web")
    root.putChild(b"plug1", PlugResource(plug1))
    root.putChild(b"plug2", PlugResource(plug2))
    root.putChild(b"plug3", PlugResource(plug3))
    root.putChild(b"plug4", PlugResource(plug4))
    root.putChild(b"plug5", PlugResource(plug5))
    root.putChild(b"plug6", PlugResource(plug6))
    root.putChild(b"inside", inside)
    root.putChild(b"outside", outside)

    site = Site(root)
    endpoint = endpoints.TCP4ServerEndpoint(reactor, 8080)
    endpoint.listen(site)

    plug1.start()
    plug2.start()
    plug3.start()
    plug4.start()
    plug5.start()
    plug6.start()

    reactor.run()
finally:
    GPIO.cleanup()
    logging.info("Garden-Pi Shutdown")
