var plugs = ["plug1", "plug2", "plug3", "plug4", "plug5", "plug6"];

function update() {
    plugs.forEach(function (entry) {
        $.ajax({
            type: 'GET',
            url: '/' + entry,
            data: {},
            dataType: 'json',
            success: function (data) {
                $.each(data, function (index, element) {
                    p = $("." + entry);
                    if (element == "on") {
                        p.addClass('on');
                        p.find('small').text("on");
                    } else if (element == "off") {
                        p.removeClass('on');
                        p.find('small').text("off");
                    } else {
                        console.log("Unknown state");
                        console.log(state);
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR)
                console.log(textStatus)
                console.log(errorThrown)
            }
        });
    });
}

function updateSensorInfo() {
    $.ajax({
        type: 'GET',
        url: '/inside',
        data: {},
        dataType: 'json',
        success: function (data) {
            $("#inside > div.temp").text(data['temp'] + "°F ~   ");
            $("#inside > div.humidity").text(data['humidity'] + "%");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });
    $.ajax({
        type: 'GET',
        url: '/outside',
        data: {},
        dataType: 'json',
        success: function (data) {
            $("#outside > div.temp").text(data['temp'] + "°F    ~   ");
            $("#outside > div.humidity").text(data['humidity'] + "%");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });
}

plugs.forEach(function (entry) {
    $("." + entry).click(function () {
        $.ajax({
            type: 'GET',
            url: '/' + entry + "/toggle",
            data: {},
            dataType: 'json',
            success: function (data) {
                console.log("success");
                $.each(data, function (index, element) {
                    p = $("." + entry);
                    if (element == "on") {
                        p.addClass('on');
                        p.find('small').text("on");
                    } else if (element == "off") {
                        p.removeClass('on');
                        p.find('small').text("off");
                    } else {
                        console.log("Unknown state");
                        console.log(state);
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR)
                console.log(textStatus)
                console.log(errorThrown)
            }
        });
    });
});

update();
updateSensorInfo();
setInterval(update, 120000);
setInterval(updateSensorInfo, 15000);